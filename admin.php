<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();
if (!$_SESSION['logged']!="admin") {
	header('Location: index.php');
}
$username = $_SESSION['uname'];

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<?php include("head.php");?>

</head>

<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
<div class="banner2">
	<div class="container">
		<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Home</a> / <span>ADMIN DASHBOARD</span></h2>
	</div>
</div>
<!-- //banner -->
<!-- single -->
		<div class="">

			<div class="col-md-8">
				<?php
					include "admin/managecategories.php";
					include "admin/managecategories_edit.php";
					include "admin/manageproducts.php";
					include "admin/manageproducts_add.php";
					include "admin/manageproducts_edit.php";
					include "admin/manageleasing.php";
					include "admin/manageleasing_add.php";
					include "admin/manageleasing_addphotos.php";
					include "admin/manageleasing_edit.php";
					include "admin/managevideos.php";
					include "admin/managevideos_add.php";
					include "admin/managevideos_edit.php";
					include "admin/selling_list.php";
					include "admin/leasing_list.php";
					include "admin/manageevents_add.php";
					include "admin/manageevents.php";
					include "admin/properties_forsale.php";
				?>
				</div> <!--end of col-8-->

			<div class="col-md-4 single-grid-right">
				<br>
				<div class="row container-fluid">
					<div class="col-md-12 text-center alert alert-success"><h3>Welcome, <?php echo strtoupper($username).'!';?></h3></div>
<!--					<div class="col-md-6"><a href="db.php?logout" class="btn btn-warning">LOGOUT</a></div>-->

				</div>
				<hr>
				<div class="blog-right1">
					<div class="" data-wow-delay=".5s">
						<div class="list-group">
							<a href="#" class="list-group-item text-center"><h3>SETUP</h3></a>
							<a href="admin.php?categories" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE CATEGORIES</a>
							<a href="admin.php?videos" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE VIDEOS</a>
							<a href="admin.php?events" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE EVENTS</a>
							<a href="admin.php?products" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE PROPERTIES FOR SALE</a>
							<a href="admin.php?leasing" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE PROPERTIES FOR LEASING</a>
							<a href="admin.php?fitout" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> MANAGE FITOUT SERVICES</a>
							<a href="admin.php?properties_forsale" class="list-group-item"><span class="glyphicon glyphicon-chevron-right"></span> PROPERTIES FOR SALE</a>
						</div>
					</div>
				</div>

			</div> <!--end of col-4-->

			<div class="clearfix"> </div>

			<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">ADD NEW CATEGORY</h4>
						</div>
						<form action="" method="post" class="form-horizontal">
							<div class="modal-body">
								<div class="container-fluid">
									<div class="form-group">
										<label for="">Category Name:</label>
										<input type="text" class="form-control" name="catname">
									</div>
									<div class="form-group">
										<label for="">Description:</label>
										<textarea name="desc" id="" cols="20" rows="10" class="form-control"></textarea>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" name="addcat" class="btn btn-primary">Save Changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			

		</div>
<!-- //single -->
<!-- footer -->
<br>
<div class="footer">
	<div class="container">
		<div class="footer-grids">
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
				<h3>About Us</h3>
				<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
					quam nihil molestiae consequatur.</p>
				<div class="social">
					<ul class="social-nav model-8">
						<li><a href="#" class="facebook"><i></i></a></li>
						<li><a href="#" class="twitter"><i> </i></a></li>
						<li><a href="#" class="g"><i></i></a></li>
						<li><a href="#" class="p"><i></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
				<h3>Subscribe</h3>
				<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis.</p>
				<form action="#" method="post">
					<input type="email" name="Email" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email...';}" required="">
					<input type="submit" value="Send">
				</form>
			</div>
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".7s">
				<h3>Latest Tweets</h3>
				<ul class="footer-grid-list">
					<li>Nam libero tempore, cum soluta nobis est eligendi optio
						cumque nihil impedit. <span>1 day ago</span></li>
					<li>Itaque earum rerum hic tenetur a sapiente delectus <a href="mailto:info@mail.com">KWEsa@mail.com</a>
						cumque nihil impedit. <span>1 day ago</span></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="footer-grids1">
			<div class="footer-grids1-left animated wow slideInLeft" data-wow-delay=".5s">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="short-codes.php">Short Codes</a></li>
					<li><a href="gallery.php">Gallery</a></li>
					<li><a href="mail.php">Mail Us</a></li>
				</ul>
			</div>
			<div class="footer-grids1-right">
				<p class="animated wow slideInRight" data-wow-delay=".5s">&copy 2016 Acreage. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 var defaults = {
		 containerID: 'toTop', // fading element id
		 containerHoverID: 'toTopHover', // fading element hover id
		 scrollSpeed: 1200,
		 easingType: 'linear'
		 };
		 */

		$().UItoTop({ easingType: 'easeOutQuart' });

	});
</script>
<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
</script>

<!-- //here ends scrolling icon -->
</body>
</html>
