<?php if (isset($_GET['listprod'])){
    $id=$_GET['listprod'];
    ?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Products</li>
    </ol>
    <?php
    $query = "SELECT catname from category where cat_id=$id";
    $out = $conn->query($query);
    $rcat = $out->fetch_assoc();
    ?>
    <h3><?php echo $rcat['catname'];?></h3>
    <br>
    <table class="table table-condensed">
        <thead>
        <tr>
            <th>#</th>
            <th>Product Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody >
        <?php
        $sql = "SELECT * FROM products WHERE cat_id=$id";
        $result = $conn->query($sql);
        $ctr = 0;
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $ctr++;
                ?>
                <tr>
                    <td><?php echo $ctr;?></td>
                    <td><?php echo  $row['prod_name'];?></td>
                    <td>
                        <a href="?editprod=<?php echo  $row['prod_id'];?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span> EDIT</a>
                        <a href="?deleteprod=<?php echo  $row['prod_id'];?>" onclick="return confirm('Are you sure you want to delete this product?');" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> DELETE</a>
                    </td>
                </tr>



                <?php

            }$ctr = 0;
        } else {
            ?>
            <tr>
                <td>No products yet.</td>
            </tr>
            <?php
        }

        ?>
        </tbody>
    </table>
<?php } ?>