<?php if (isset($_GET['leasing'])){?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Products for Leasing</li>
    </ol>
    <div>

        <div class="bs-docs-example animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
            <a href="" class="btn btn-info" data-toggle="modal" data-target=".addlease"><span class="glyphicon glyphicon-plus"></span> Add Product for Leasing</a>

            <?php
            $query = "SELECT * FROM category";
            $rs = $conn->query($query);
            while($ro = $rs->fetch_assoc()){
                ?>
                <br><br>
                <?php
                $cat=$ro['cat_id'];
                $q = "SELECT * from leasing WHERE cat_id=$cat";
                $r = $conn->query($q);
                $num = $r->num_rows;
                ?>
                <h4><?php echo $ro['catname'];?> <a href="admin.php?listlease=<?php echo $ro['cat_id'];?>"> [ <?php echo $num;?> ] [VIEW ALL]</a></h4>
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                    <?php
                    $sql = "SELECT * FROM leasing WHERE cat_id=$cat LIMIT 5";
                    $result = $conn->query($sql);
                    $ctr = 0;
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            $ctr++;
                            ?>
                            <tr>
                                <td><?php echo $ctr;?></td>
                                <td><?php echo  $row['prod_name'];?></td>
                                <td><?php echo  $row['prod_address'];?></td>
                                <td>
                                    <a href="?addphotos=<?php echo  $row['prod_id'];?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-plus"></span> ADD PHOTO</a>
                                    <a href="?editleasing=<?php echo  $row['prod_id'];?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span> EDIT</a>
                                    <a href="?deleteleasing=<?php echo  $row['prod_id'];?>" onclick="return confirm('Are you sure you want to delete this product?');" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> DELETE</a>
                                </td>
                            </tr>



                            <?php

                        }$ctr = 0;
                    } else {
                        ?>
                        <tr>
                            <td>No products for leasing yet.</td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>

            <?php } ?>
        </div>
    </div>
<?php } ?>