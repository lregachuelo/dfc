<?php if (isset($_GET['events'])){?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Events</li>
    </ol>
    <div>

        <div class="bs-docs-example animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
            <span class="test-warning"><?php echo $msg;?></span>
            <a href="?addevent" class="btn btn-info"><span class="glyphicon glyphicon-plus"></span> Add Event</a>


                <br><br>
                <h4>EVENTS LIST</h4>
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Event Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Date</th>
                        <th>Venue</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody >
                    <?php
                    $sql = "SELECT * FROM events";
                    $result = $conn->query($sql);
                    $ctr = 0;
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            $ctr++;
                            ?>
                            <tr>
                                <td><?php echo $ctr;?></td>
                                <td><?php echo  $row['title'];?></td>
                                <td><?php echo  $row['timefrom'];?></td>
                                <td><?php echo  $row['timeto'];?></td>
                                <td><?php echo  $row['date'];?></td>
                                <td><?php echo  $row['venue'];?></td>
                                <td>
                                    <a href="?addphotos=<?php echo  $row['prod_id'];?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-plus"></span> ADD PHOTO</a>
                                    <a href="?editevent=<?php echo  $row['prod_id'];?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-edit"></span> EDIT</a>
                                    <a href="?deleteevent=<?php echo  $row['id'];?>" onclick="return confirm('Are you sure you want to delete this product?');" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> DELETE</a>
                                </td>
                            </tr>



                            <?php

                        }$ctr = 0;
                    } else {
                        ?>
                        <tr>
                            <td>No events created.</td>
                        </tr>
                        <?php
                    }

                    ?>
                    </tbody>
                </table>

            <?php } ?>
        </div>
    </div>