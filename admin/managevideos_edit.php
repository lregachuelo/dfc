<?php

if(isset($_POST['updatevid'])) {
    $pid = $_GET['editvid'];
    $pic=$_POST['wew'];
    $target = "upload/";
    $target = $target . basename( $_FILES['uploaded']['name']) ;
    $name= $_FILES['uploaded']['name'];
    $ok=1;
    $dot = strripos($name,".");
    $ext = substr($name,$dot);
    if ($ext ==".jpg" || $ext ==".jpeg" ||  $ext ==".png" || $ext ==".PNG" || $ext ==".gif" || $ext ==".JPG" || $ext ==".GIF" || $ext ==".JPEG"){

        if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target))
        {
            $query = "UPDATE videos SET vid_thumbpic='$name', vid_title='$_POST[vid_title]', vid_detail='$_POST[vid_detail]',  vid_link='$_POST[vid_link]' WHERE vid_id='$pid'";
            $res = $conn->query($query);
            ?>
            <script>
                alert('Successfully updated!');
                window.location.href='admin.php?videos';
            </script>
            <?php
        }

    }else{
        $query = "UPDATE videos SET vid_thumbpic='$name', vid_title='$_POST[vid_title]', vid_detail='$_POST[vid_detail]',  vid_link='$_POST[vid_link]' WHERE vid_id='$pid'";
        $res = $conn->query($query);
        ?>
        <script>
            alert('Successfully updated!');
            window.location.href='admin.php?videos';
        </script>
        <?php
    }
}
if (isset($_GET['editvid'])){ ?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Edit Video</li>
    </ol>
    <h3>Edit Videos</h3>
    <br>
    <div class="row">
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <?php
            $id = $_GET['editvid'];
            $sql = "SELECT * FROM videos where vid_id='$id'";
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc()){
            ?>


            <div class="col-md-4">
                <img src="upload/<?php echo $row['vid_thumbpic'];?>" alt="" height="250" width="250">

            </div>
            <div class="col-md-8">

                <div class="container-fluid">
                    <div class="form-group">
                        <label for="">Preview Image:</label>
                        <input type="file" class="form-control" name="uploaded">
                    </div>

                    <div class="form-group">
                        <label for="">Video Title:</label>
                        <input type="text" class="form-control" name="vid_title" value="<?php echo $row['vid_title'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Description / Content:</label>
                        <textarea name="vid_detail" id="" cols="20" rows="10" class="form-control"><?php echo $row['vid_detail'];?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Video Link / URL:</label>
                        <input type="text" class="form-control" name="vid_link" value="<?php echo $row['vid_link'];?>">
                    </div>



                    <input type="submit" name="updatevid" value="Save" class="btn btn-success" >
                    <a href="" onclick="window.history.back();" class="btn btn-warning">Cancel</a>
                </div>

                <?php
                }
                ?>
        </form>
    </div>
    </div>
<?php } ?>