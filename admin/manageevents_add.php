<?php
    if(isset($_FILES['image'])){
        $title = $_POST['title'];
        $timefrom = $_POST['timefrom'];
        $from = date('g:i a', strtotime($timefrom));
        $timeto = $_POST['timeto'];
        $to  = date('g:i a', strtotime($timeto));
        $date = $_POST['date'];

        $dt = date('d/m/Y', strtotime(str_replace('.', '-', $date)));
//        $finaldate = date('F d, Y', $dt);
        $venue = $_POST['venue'];
        $details = $_POST['details'];
        $errors= array();
        $file_name = $_FILES['image']['name'];
        $file_size =$_FILES['image']['size'];
        $file_tmp =$_FILES['image']['tmp_name'];
        $file_type=$_FILES['image']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

        $expensions= array("jpeg","jpg","png");

        if($file_size > 5097152){
            $errors[]='File size must be excately 2 MB';
        }

        if(in_array($file_ext,$expensions)=== false){
            $errors[]="extension not allowed, please choose a JPEG or PNG file.";
        }



        if(empty($errors)==true){
            move_uploaded_file($file_tmp,"events/".$file_name);
            $sql = "insert into events (image, title, timefrom, timeto, date, venue, details, addedby) VALUES ('$file_name', '$title', '$from', '$to', '$dt', '$venue', '$details', '$_SESSION[uid]')";
            $rs = $conn->query($sql);
            $errors[] = "Success.";
            $msgs = array($errors);
        }else{
            $msgs = array($errors);
        }
    }


if (isset($_GET['addevent'])){?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Events</li>
    </ol>
    <div class=""><?php
        $msg = $msgs;
        $imp[] = implode(',', $msg);
        foreach ($msg as  $key => $values){
            echo $values[$key];
        }
                  ?>
    </div>
    <div>
        <!--					<button TYPE="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">ADD NEW CATEGORY</button>-->
        <div class="bs-docs-example animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
            <form action="" method="POST" enctype="multipart/form-data" class="form-horizontal container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Upload Image: </label>
                            <input type="file" name="image" />
                        </div>
                        <div class="form-group">
                            <label for="">Event Name: </label>
                            <input type="text" name="title" placeholder="Event title..." class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Start Time: </label>
                            <input type="time" class="form-control" name="timefrom">
                        </div>
                        <div class="form-group">
                            <label for="">End Time: </label>
                            <input type="time" class="form-control" name="timeto">
                        </div>
                        <div class="form-group">
                            <label for="">Date: </label>
                            <input type="date" name="date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Venue: </label>
                            <input type="text" class="form-control" name="venue">
                        </div>
                        <div class="form-group">
                            <label for="">Details:</label>
                            <textarea name="details" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Save" class="btn btn-primary">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php } ?>