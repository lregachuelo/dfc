<?php

if(isset($_POST['updateprod'])) {
    $school = addslashes($_POST['prod_school']);
    $transpo = addslashes($_POST['prod_transpo']);
    $sm = addslashes($_POST['prod_sm']);
    $mall = addslashes($_POST['prod_mall']);
    $resto = addslashes($_POST['prod_resto']);
    $hospital = addslashes($_POST['prod_hospital']);
    $church = addslashes($_POST['prod_church']);
    $prodname = addslashes($_POST['prod_name']);
    $address = addslashes($_POST['prod_address']);
    $pid = $_GET['editprod'];
    $pic=$_POST['wew'];
    $target = "upload/";
    $target = $target . basename( $_FILES['uploaded']['name']) ;
    $name= $_FILES['uploaded']['name'];
    $ok=1;
    $dot = strripos($name,".");
    $ext = substr($name,$dot);
    if ($ext ==".jpg" || $ext ==".jpeg" ||  $ext ==".png" || $ext ==".PNG" || $ext ==".gif" || $ext ==".JPG" || $ext ==".GIF" || $ext ==".JPEG"){

        if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target))
        {
            $query = "UPDATE products SET prod_image='$name', prod_name='$prodname', cat_id='$_POST[cat_id]', prod_address='$address', prod_price='$_POST[prod_price]', prod_desc='$_POST[prod_desc]', prod_school='$school', prod_transpo='$transpo', prod_sm='$sm', prod_mall='$mall', prod_resto='$resto', prod_hospital='$hospital', prod_church='$church' WHERE prod_id='$pid'";
            $res = $conn->query($query);
            ?>
            <script>
                alert('Successfully updated!');
                window.location.href='admin.php?editprod=<?php echo $pid; ?>';
            </script>
            <?php
        }

    }else{
        $query2 = "UPDATE products SET  prod_name='$prodname', cat_id='$_POST[cat_id]', prod_address='$address', prod_price='$_POST[prod_price]', prod_desc='$_POST[prod_desc]', prod_school='$school', prod_transpo='$transpo', prod_sm='$sm', prod_mall='$mall', prod_resto='$resto', prod_hospital='$hospital', prod_church='$church' WHERE prod_id='$pid'";
        $res2 = $conn->query($query2);
        ?>
        <script>
            alert('Successfully updated!');
            window.location.href='admin.php?editprod=<?php echo $pid; ?>';
        </script>
        <?php
    }
}

if (isset($_GET['editprod'])){ ?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Edit Product</li>
    </ol>

    <div class="row">
        <div class="col-md-6"><h3>Edit Product</h3></div>
        <div class="col-md-6"><a href="admin.php?products" class="btn btn-warning pull-right">BACK TO LIST</a></div>
    </div>


    <br>
    <div class="row">
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <?php
            $id = $_GET['editprod'];
            $sql = "SELECT * FROM products where prod_id='$id'";
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc()){
            ?>


            <div class="col-md-4">
                <img src="upload/<?php echo $row['prod_image'];?>" alt="" height="250" width="250">

            </div>
            <div class="col-md-8">

                <div class="container-fluid">
                    <div class="form-group">
                        <label for="">Select Image:</label>
                        <input type="file" class="form-control" name="uploaded" value="<?php echo $row['prod_image'];?>" />
                        <input type="hidden" name="wew" value="<?php echo $row['prod_image']; ?>"/>
                    </div>

                    <div class="form-group">
                        <label for="">Product Name:</label>
                        <input type="text" class="form-control" name="prod_name" value="<?php echo $row['prod_name'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Product Category:</label>
                        <select name="cat_id" id="cat_id" class="form-control">
                            <?php $q = "select * from category";
                            $result = $conn->query($q);
                            while($a = $result->fetch_assoc()){ ?>
                                <option value="<?php echo $a['cat_id'];?>" name="cat_id" <?php if ($row['cat_id'] == $a['cat_id']) echo "selected";?>><?php echo $a['catname']?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Address:</label>
                        <input type="text" class="form-control" name="prod_address" value="<?php echo $row['prod_address'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Price:</label>
                        <input type="text" class="form-control" name="prod_price" value="<?php echo $row['prod_price'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Description / Content:</label>
                        <textarea name="prod_desc" id="" cols="20" rows="10" class="form-control"><?php echo $row['prod_desc'];?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="" class="text-muted text-warning">Places Nearby: [input data separated by comma]</label>
                    </div>

                    <div class="form-group">
                        <label for="">School:</label>
                        <input type="text" class="form-control" name="prod_school" value="<?php  echo $row['prod_school'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Transportation:</label>
                        <input type="text" class="form-control" name="prod_transpo" value="<?php  echo $row['prod_transpo'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Supermarkets:</label>
                        <input type="text" class="form-control" name="prod_sm" value="<?php echo  $row['prod_sm'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Malls:</label>
                        <input type="text" class="form-control" name="prod_mall" value="<?php  echo $row['prod_mall'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Restaurants:</label>
                        <input type="text" class="form-control" name="prod_resto" value="<?php  echo $row['prod_resto'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Hospital:</label>
                        <input type="text" class="form-control" name="prod_hospital" value="<?php  echo $row['prod_hospital'];?>">
                    </div>

                    <div class="form-group">
                        <label for="">Church:</label>
                        <input type="text" class="form-control" name="prod_church" value="<?php echo  $row['prod_church'];?>">
                    </div>



                    <input type="submit" name="updateprod" value="Save" class="btn btn-success" >
                    <a href="admin.php?products" class="btn btn-warning">Cancel</a>
                </div>

                <?php
                }
                ?>
        </form>
    </div>
    </div>
<?php } ?>