<?php
if(isset($_POST['savephoto'])){
    $prodid = $_POST['prodid'];
    $target = "photo/";
    $target = $target . basename( $_FILES['uploaded']['name']) ;
    $name= $_FILES['uploaded']['name'];
    $ok=1;
    $type = $_FILES['uploaded']['type'];
    $name = $_FILES['uploaded']['name'];
    $dot = strripos($name,".");
    $ext = substr($name,$dot);

    if (file_exists($target)) {
        echo "<script>alert('Sorry, file already exists.');</script>";
        $uploadOk = 0;
    }

    if ($uploadOk==0){
        echo "<script>alert('Sorry, your file was not uploaded.');</script>";
    }else {

        if ($ext == ".jpg" || $ext == ".jpeg" || $ext == ".png" || $ext == ".PNG" || $ext == ".gif" || $ext == ".JPG" || $ext == ".GIF" || $ext == ".JPEG") {
            if (move_uploaded_file($_FILES['uploaded']['tmp_name'], $target)) {
                $sql = "INSERT INTO leasing_images
								(img_name, img_pid)
						  VALUES
						  		('$name', '$prodid')
					";
                $result = $conn->query($sql);
                echo "<script>alert('Successfully added!');</script>";

            } else {

                echo "<script>alert('Upload failed, please check your file.');</script>";

            }
        } else {
            ?>
            <script>
                alert('Please upload a picture.');
                window.location.href = 'admin.php?leasing';
            </script>
            <?php
        }
    }
}


if (isset($_GET['addphotos'])){
    $prod = $_GET['addphotos'];
    ?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Add Photos</li>
    </ol>
    <h3>Add Photos</h3>
    <br>
    <div class="row">
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
            <div class="col-md-4">
                <label for="">Add Image Here</label>
                <input type="file" name="uploaded" class="form-control">
                <input type="hidden" name="prodid" value="<?php echo $prod;?>" class="form-control">
            </div>
            <div class="col-md-2">
                <br>
                <button type="submit" name="savephoto" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!--						--><?php
            //							$sql = "SELECT * from leasing where prod_id='$prod'";
            //							$res = $conn->query($sql);
            //							if($res->num_rows > 0){
            //								while ($row = $res->fetch_assoc()) {
            //									?>
            <!--									<br>-->
            <!--									<img src="upload/--><?php //echo $row['prod_image'];?><!--" alt="" width="200" height="200" class="img img-rounded">-->
            <!---->
            <!--									--><?php
            //								}
            //							}else{
            //								echo "No photos yet. ";
            //							}
            //
            //						?>
            <br>
            <?php
            $pr = $_GET['addphotos'];
            $sql2 = "SELECT * from leasing_images where img_pid='$pr'";
            $res2 = $conn->query($sql2);
            if($res2->num_rows > 0){
                while ($row = $res2->fetch_assoc()) {
                    ?>

                    <img src="photo/<?php echo $row['img_name'];?>" alt="" width="200" height="200" class="img img-rounded">

                    <?php
                }
            }else{
                echo "No photos yet. ";
            }

            ?>
        </div>
    </div>
    </div>
<?php } ?>