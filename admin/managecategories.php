<?php if (isset($_GET['categories'])){?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Categories</li>
    </ol>
    <div>
        					<button TYPE="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">ADD NEW CATEGORY</button>
        <div class="bs-docs-example animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
            <form action="" method="post">
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    $sql = "SELECT * FROM category";
                    $result = $conn->query($sql);
                    $ctr = 0;
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            $ctr++; ?>
                            <tr>
                                <td><?php echo $ctr;?></td>
                                <td><?php echo  $row['catname'];?></td>
                                <td>
                                    <input type="hidden" name="ed" value="<?php echo $row['cat_id'];?>">
                                    <a href="?edit=<?php echo  $row['cat_id'];?>" class="btn btn-xs btn-primary"> EDIT</a>
                                    <!--														<a href="?delete=--><?php //echo  $row['cat_id'];?><!--" onclick="return confirm('Delete category?');" class="btn btn-xs btn-danger"> delete</a>-->
                                </td>
                            </tr>
                            <?php
                        }$ctr = 0;
                    } else {
                        echo "No categories created.";
                    } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
<?php } ?>