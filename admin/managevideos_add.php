<?php
if(isset($_POST['addvid'])){

    $vidname = $_POST['vid_title'];
    $detail = $_POST['vid_detail'];
    $link = $_POST['vid_link'];


    $target = "videos/";
    $target = $target . basename( $_FILES['uploaded']['name']) ;
    $name= $_FILES['uploaded']['name'];
    $ok=1;
    $type = $_FILES['uploaded']['type'];
    $name = $_FILES['uploaded']['name'];
    $dot = strripos($name,".");
    $ext = substr($name,$dot);

    if ($ext ==".jpg" || $ext ==".jpeg" ||  $ext ==".png" || $ext ==".PNG" || $ext ==".gif" || $ext ==".JPG" || $ext ==".GIF" || $ext ==".JPEG"){
        if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target)){
            $sql = "insert into `videos` 
								(`vid_title`,`vid_detail`,`vid_link`,`vid_thumbpic`,`vid_dateadded`)
								values
								('$vidname','$detail','$link','$name',CURDATE())";
            $result = $conn->query($sql);
            ?>
            <script>
                alert('Successfully added.');
                window.location.href='admin.php?videos';
            </script>
            <?php

        }
        else {

            echo "<script>alert('Upload failed, please check your file.');</script>";

        }
    }
    else{
        ?>
        <script>
            alert('Please upload a picture.');
            window.location.href='admin.php?videos';
        </script>
        <?php
    }
}
?>
<div class="modal fade addvideo" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" class="modal-dialog" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD NEW VIDEO</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">
                            <label for="">Preview Image:</label>
                            <input type="file" class="form-control" name="uploaded">
                        </div>

                        <div class="form-group">
                            <label for="">Video Title:</label>
                            <input type="text" class="form-control" name="vid_title">
                        </div>

                        <div class="form-group">
                            <label for="">Description / Content:</label>
                            <textarea name="vid_detail" id="" cols="20" rows="10" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Video Link / URL:</label>
                            <input type="text" class="form-control" name="vid_link">
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="addvid" class="btn btn-primary">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>