<?php
if(isset($_POST['addleasing'])) {

    $school = addslashes($_POST['prod_school']);
    $transpo = addslashes($_POST['prod_transpo']);
    $sm = addslashes($_POST['prod_sm']);
    $mall = addslashes($_POST['prod_mall']);
    $resto = addslashes($_POST['prod_resto']);
    $hospital = addslashes($_POST['prod_hospital']);
    $church = addslashes($_POST['prod_church']);
    $prodname = addslashes($_POST['prod_name']);
    $address = addslashes($_POST['prod_address']);

    $target_dir = "leasing/";
    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
    $name= $_FILES['fileToUpload']['name'];
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);


    // Check if file already exists
    if (file_exists($target_file)) {
        echo "<script>alert('Sorry, file already exists.');</script>";
        $uploadOk = 0;
    }

// Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
        echo "<script>alert('Sorry, only JPG, JPEG, PNG & GIF files are allowed.');</script>";

        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "<script>alert('Sorry, your file was not uploaded.');</script>";

// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

            $q2 = "SELECT * FROM leasing WHERE prod_name='$prodname'";
            $run = $conn->query($q2);
            if($run -> num_rows > 0){
                echo "<script>alert('Product already exists!');</script>";
            }else {
                $query = "INSERT INTO leasing
                              (prod_image, prod_name, cat_id, prod_address, prod_price, prod_desc, prod_school, prod_transpo, prod_sm, prod_mall, prod_resto, prod_hospital, prod_church)
                              VALUES
                              ('$name', '$prodname', '$_POST[cat_id]', '$address', '$_POST[prod_price]', '$_POST[prod_desc]', '$school', '$transpo', '$sm', '$mall',  '$resto', '$hospital', '$church')
                  ";
                $res = $conn->query($query);
                echo "<script>alert('Successfully added!');</script>";
            }
        } else {
            echo "<script>alert('Sorry, there was an error uploading your file.');</script>";


        }
    }
}
?>

<div class="modal fade addlease" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" class="modal-dialog" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD NEW PRODUCT</h4>
            </div>
            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body">
                    <div class="container-fluid">

                        <div class="form-group">
                            <label for="">Select Image:</label>
                            <input type="file" class="form-control" name="fileToUpload">
                        </div>

                        <div class="form-group">
                            <label for="">Product Name:</label>
                            <input type="text" class="form-control" name="prod_name">
                        </div>

                        <div class="form-group">
                            <label for="">Product Category:</label>
                            <select name="cat_id" id="cat_id" class="form-control">
                                <?php $q = "select * from category";
                                $result = $conn->query($q);
                                while($a = $result->fetch_assoc()){ ?>
                                    <option value="<?php echo $a['cat_id'];?>" name="cat_id"><?php echo $a['catname']?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Address:</label>
                            <input type="text" class="form-control" name="prod_address">
                        </div>

                        <div class="form-group">
                            <label for="">Price:</label>
                            <input type="text" class="form-control" name="prod_price">
                        </div>

                        <div class="form-group">
                            <label for="">Description / Content:</label>
                            <textarea name="prod_desc" id="" cols="20" rows="10" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="" class="text-muted text-warning">Places Nearby: [input data separated by comma]</label>
                        </div>

                        <div class="form-group">
                            <label for="">School:</label>
                            <input type="text" class="form-control" name="prod_school">
                        </div>

                        <div class="form-group">
                            <label for="">Transportation:</label>
                            <input type="text" class="form-control" name="prod_transpo">
                        </div>

                        <div class="form-group">
                            <label for="">Supermarkets:</label>
                            <input type="text" class="form-control" name="prod_sm">
                        </div>

                        <div class="form-group">
                            <label for="">Malls:</label>
                            <input type="text" class="form-control" name="prod_mall">
                        </div>

                        <div class="form-group">
                            <label for="">Restaurants:</label>
                            <input type="text" class="form-control" name="prod_resto">
                        </div>

                        <div class="form-group">
                            <label for="">Hospital:</label>
                            <input type="text" class="form-control" name="prod_hospital">
                        </div>

                        <div class="form-group">
                            <label for="">Church:</label>
                            <input type="text" class="form-control" name="prod_church">
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="addleasing" class="btn btn-primary">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>