<?php if (isset($_GET['videos'])){?>
    <br>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Setup</a></li>
        <li class="active">Manage Videos</li>
    </ol>
    <div>
        <button TYPE="button" class="btn btn-primary" data-toggle="modal" data-target=".addvideo">ADD NEW VIDEO</button>
        <div class="bs-docs-example animated wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="500ms">
            <form action="" method="post">
                <?php
                $q = "SELECT * FROM videos";
                $r = $conn->query($q);
                $total = $r->num_rows;
                echo "Total: ".$total;
                ?>
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Video Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <?php
                    $sql = "SELECT * FROM videos";
                    $result = $conn->query($sql);

                    $count = $result->num_rows;
                    $ctr = 0;
                    if ($result->num_rows > 0) {
                    // output data of each row
                    while($row = $result->fetch_assoc()) {
                    $ctr++; ?>


                    <tbody>
                    <tr>
                        <td><?php echo $ctr;?></td>
                        <td><?php echo  $row['vid_title'];?></td>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $row['vid_id'];?>">
                            <a href="?editvid=<?php echo  $row['vid_id'];?>" class="btn btn-xs btn-primary"> EDIT</a>
                            <a href="?deletevid=<?php echo  $row['vid_id'];?>" onclick="return confirm('Are you sure you want to delete this entry?');" class="btn btn-xs btn-danger"> DELETE</a>
                            <!--														<a href="?delete=--><?php //echo  $row['cat_id'];?><!--" onclick="return confirm('Delete category?');" class="btn btn-xs btn-danger"> delete</a>-->
                        </td>
                    </tr>
                    <?php
                    }$ctr = 0;
                    } else {
                        echo "No records found.";
                    } ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
<?php } ?>