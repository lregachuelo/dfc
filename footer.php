<br><br><br>
<div class="footer">
	<div class="container">

		<div class="footer-grids1">
			<div class="footer-grids1-left animated wow slideInLeft" data-wow-delay=".5s">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="productsandservices.php">Products for Sale</a></li>
					<li><a href="leasing.php?viewlist">Leasing Services</a></li>
					<li><a href="fitoutservices.php">Fitout Services</a></li>
					<li><a href="about.php?whoweare">About Us</a></li>
				</ul>
			</div>
			<div class="footer-grids1-right">
				<p class="animated wow slideInRight" data-wow-delay=".5s">&copy 2016 Domus Fortem Corporation. All rights reserved.</p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>