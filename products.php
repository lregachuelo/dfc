<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();
//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<?php include("head.php");?>

</head>

<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
<div class="banner1">
	<div class="container">
		<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Home</a> / <span>Products & Services</span></h2>
	</div>
</div>
<!-- //banner -->
<!-- single -->
		<div class="">

			<div class="col-md-8">

				<?php if(isset($_GET['viewprod'])){
					$pid = $_GET['viewprod'];
					$sql = "SELECT * FROM products where prod_id='$pid'";
					$result = $conn->query($sql);
					while($row = $result->fetch_assoc()){
					?>
					<br>
					<br>
					<div class="flex-slider-single animated wow slideInLeft" data-wow-delay=".5s">
						<section class="slider">
							<div class="flexslider">
								<ul class="slides">
									<li>
										<div class="single-grid-left-grid">
											<img src="upload/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="height: 500px" />
											<div class="single-grid-left-grid1">
												<div class="single-grid-left-grid1-left">
													<h3><i class="glyphicon glyphicon-star"></i> <span> <?php echo $row['prod_price'];?></span></h3>
												</div>
												<div class="single-grid-left-grid1-right">
													<h4><?php echo $row['prod_name'];?></h4>
													<p> <?php echo $row['prod_address'];?></p>
												</div>
												<div class="clearfix"> </div>
												<p class="fugiat text-justify">

													<?php echo $row['prod_desc'];?>
												</p>

												PLACES NEARBY:
												<ul>
													<!--													<li>PLACES NEARBY</li>-->
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_school'];?>"
															data-html="true"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>School</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_transpo'];?>"
															data-html="true"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>Transpo</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_sm'];?>"
															data-html="true"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Supermarket</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_mall'];?>"
															data-html="true"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span>Malls</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_resto'];?>"
															data-html="true"><span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span>Resto</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_hospital'];?>"
															data-html="true"><span class="glyphicon glyphicon-bed" aria-hidden="true"></span>Hospital</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_church'];?>"
															data-html="true"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>Church</a>
													</li>

												</ul>
											</div>
										</div>
									</li>
							</div>
						</section>
						<!--FlexSlider-->
						<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
						<script defer src="js/jquery.flexslider.js"></script>
						<script type="text/javascript">
							$(window).load(function(){
								$('.flexslider').flexslider({
									animation: "slide",
									start: function(slider){
										$('body').removeClass('loading');
									}
								});
							});
						</script>
						<!--End-slider-script-->
					</div>

					<br><br>
					<?php  } ?>
				<?php  } ?>

				<?php if(isset($_GET['viewlist'])){
					$cid = $_GET['viewlist'];
					$sql1 = "SELECT catname FROM category where cat_id='$cid'";
					$rs = $conn->query($sql1);
					?>
				<div class="popular-posts" id="cbd">
					<?php while ($r = $rs->fetch_assoc()){ ?>
					<h3 class="animated wow zoomIn text-center" data-wow-delay=".5s"><?php echo $r['catname'];?></h3>
					<?php } ?>
					<?php
					$sql = "SELECT * FROM products WHERE cat_id='$cid'";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
					// output data of each row
					while($row = $result->fetch_assoc()) {
					?>
					<div class="popular-posts-grids">


							<div class="col-md-4 popular-posts-grid animated wow slideInLeft" data-wow-delay=".5s">
								<div class="popular-posts-grid1">
									<a href="products.php?viewprod=<?php echo $row['prod_id'];?>"><img src="upload/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="width: 640px;height: 205px;"  /></a>
									<h4><a href="products.php?viewprod=<?php echo $row['prod_id'];?>"><?php echo strtoupper($row['prod_name']) ;?></a></h4>
									<p class="text-center" style="height:75px;"><?php echo $row['prod_address'];?> <br><br></p>
								</div>
							</div>

					</div>
							<?php } ?>
						<?php }else{
							?>
							<p class="animated wow slideInLeft">
								No record found in this category.
							</p>
							<?php
								} ?>

				</div>
				<?php } ?>

			</div>

			<div class="col-md-4 single-grid-right">

				<br><br>
				<div class=" mail-grid-right animated wow slideInRight container-fluid " style="background-color: #c1e2b3"data-wow-delay=".5s">
					<div class="mail-grid-right1"  style="background-color: #c1e2b3">
						<h3>QUICK NAVIGATION</h3>
						<hr>
				<?php
				$query = "SELECT * FROM category";
				$rs = $conn->query($query);
				while($r = $rs->fetch_assoc()){
				?>


							<h4 class="text-left"><a href="products.php?viewlist=<?php echo $r['cat_id'];?>"><?php echo $r['catname'];?></a></h4>
							<select name="" id="" onChange="document.location = this.value" value="GO" class="form-control">
								<?php
								$catid = $r['cat_id'];
								$sql = "SELECT * FROM products WHERE cat_id='$catid'";
								$result = $conn->query($sql);
								if ($result->num_rows > 0) {
									?>
									<option >--SELECT ITEM--</option>

									<?php
									while($row = $result->fetch_assoc()) {
										?>
										<option value="products.php?viewprod=<?php echo $row['prod_id'];?>" <?php if($_GET['viewprod'] == $row['prod_id']){ ?> selected <?php } ?>><?php echo $row['prod_name'];?></option>

										<?php
									}
								} else {
									?>
									<option value="" >Nothing to display</option>
									<?php
								} ?>
							</select>



				<?php } ?>
						</div>
					</div>


				<br><br>

				<div class=" mail-grid-right animated wow slideInRight container-fluid " style="background-color: #444444" data-wow-delay=".5s">
					<div class="mail-grid-right1"  style="background-color: #444444">
						<img src="images/mandy.png" alt=" " class="img-responsive" />
						<h4 style="color: #00ffb9">Normandy Fresnido <span>Contact Person</span></h4>
						<br>
						<ul class="phone-mail">
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +63927-880-2393 | +63920-559-7833 </li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:mandyfresnido@domusfortem.com">mandyfresnido@domusfortem.com</a></li>
						</ul>

					</div>
				</div>



			</div>
			<div class="clearfix"> </div>
		</div>
<!-- //single -->
<!-- footer -->
<br>
<br>
<br>
<br>
<br>
<div class="footer">
	<div class="container">
		<div class="footer-grids">
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
				<h3>About Us</h3>
				<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
					quam nihil molestiae consequatur.</p>
				<div class="social">
					<ul class="social-nav model-8">
						<li><a href="#" class="facebook"><i></i></a></li>
						<li><a href="#" class="twitter"><i> </i></a></li>
						<li><a href="#" class="g"><i></i></a></li>
						<li><a href="#" class="p"><i></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
				<h3>Subscribe</h3>
				<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis.</p>
				<form action="#" method="post">
					<input type="email" name="Email" value="Enter Your Email..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email...';}" required="">
					<input type="submit" value="Send">
				</form>
			</div>
			<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".7s">
				<h3>Latest Tweets</h3>
				<ul class="footer-grid-list">
					<li>Nam libero tempore, cum soluta nobis est eligendi optio
						cumque nihil impedit. <span>1 day ago</span></li>
					<li>Itaque earum rerum hic tenetur a sapiente delectus <a href="mailto:info@mail.com">KWEsa@mail.com</a>
						cumque nihil impedit. <span>1 day ago</span></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
		<div class="footer-grids1">
			<div class="footer-grids1-left animated wow slideInLeft" data-wow-delay=".5s">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="about.php">About Us</a></li>
					<li><a href="short-codes.php">Short Codes</a></li>
					<li><a href="gallery.php">Gallery</a></li>
					<li><a href="mail.php">Mail Us</a></li>
				</ul>
			</div>
			<div class="footer-grids1-right">
				<p class="animated wow slideInRight" data-wow-delay=".5s">&copy 2016 Acreage. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 var defaults = {
		 containerID: 'toTop', // fading element id
		 containerHoverID: 'toTopHover', // fading element hover id
		 scrollSpeed: 1200,
		 easingType: 'linear'
		 };
		 */

		$().UItoTop({ easingType: 'easeOutQuart' });

	});

</script>
<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
</script>

<!-- //here ends scrolling icon -->
</body>
</html>