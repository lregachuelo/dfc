<?php
include "db.php";
session_start();
//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<!-- for-mobile-apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Domus Fortem Corporation" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>

	<!-- //for-mobile-apps -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<!-- //js -->
	<!-- gallery -->
	<link href='css/simplelightbox.css' rel='stylesheet' type='text/css'>
	<!-- //gallery -->
	<!-- animation-effect -->
	<link href="css/animate.min.css" rel="stylesheet">
	<script src="js/wow.min.js"></script>
	<script>
		new WOW().init();
	</script>
	<!-- //animation-effect -->
	<link href='//fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
	<link rel="manifest" href="images/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
	<style>
		@import 'https://fonts.googleapis.com/css?family=Kaushan+Script|Viga';
		/*font-family: 'Kaushan Script', cursive;*/
		/*font-family: 'Viga', sans-serif;*/
	</style>
	<!-- start-smoth-scrolling -->

</head>

<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
<div class="company">
	<div class="container">
		<br><br>
		<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Company Profile</a></h2>
	</div>
</div>
<div>

	<br><br>

	<h3 class="title usefont animated wow zoomIn" id="whoweare" data-wow-delay=".5s">Who We Are</h3>
	<p class="qui animated wow zoomIn" data-wow-delay=".5s">Get to know more about our company, what we do and what we can do.</p>


	<br><br>

	<div class="row">
		<div class="col-md-4 animated slideInDown outline">
			<p class="text-center" style="font-size: 800%; color: #00ffb9"><i class="glyphicon glyphicon-check"></i></p>
		</div>
		<div class="col-md-4 animated slideInUp outline">
			<p class="text-center" style="font-size: 800%; color: orange"><i class="glyphicon glyphicon-level-up"></i></p>
		</div>
		<div class="col-md-4 animated slideInDown outline ">
			<p class="text-center" style="font-size: 800%; color: #00ffb9"><i class="glyphicon glyphicon-link"></i></p>
		</div>
	</div>

	
	<div class="row">
		<div class="col-md-4 animated slideInDown outline">
			<h3	 class="text-center" style="color: inherit;">Registered with the SEC in May 2016</h3>
		</div>
		<div class="col-md-4 animated slideInUp outline">
			<h3	 class="text-center" style="color: inherit;">
				Provides real estate services in Property Consultancy, Property Management, Leasing Services, Buying & Selling of Property
			</h3>
		</div>
		<div class="col-md-4 animated slideInDown outline ">
			<h3	 class="text-center" style="color: inherit;">Affiliate of Vista Land Marketing Pte Ltd in Singapore</h3>
		</div>
	</div>

	<br><br><br>
	<br><br><br>
	<hr>
	<br><br>
	<h3 class="title usefont animated wow zoomIn" data-wow-delay=".5s">What We Offer</h3>
	<br><br>
	<div class="row text-center">
		<div class="col-md-4 ">
			<div class="container-fluid">
				<h2 id="fnt2">BMS</h2><h3 id="fnt">Basic Maintenance System</h3>
				<p>&bull; Regular checks on electrical, plumbing, drainage, structure conditions, appliances, furniture, home accessories</p>
				<p>&bull; Occasional repair and/or additional work, as needed</p>
			</div>
		</div>
		<div class="col-md-4 ">
			<div class="container-fluid">
				<h2 id="fnt2">LMS</h2><h3 id="fnt">Leasing and Marketing System</h3>
				<p>&bull; Short term	–	daily/weekly/monthly</p>
				<p>&bull; Long term	–	6 months or more</p>
			</div>
		</div>
		<div class="col-md-4 ">
			<div class="container-fluid">
				<h2 id="fnt2">HOS</h2><h3 id="fnt">Hotel Operation System</h3>
				<p>&bull; Basic Maintenance</p>
				<p>&bull; Housekeeping</p>
				<p>&bull; Check-in and check-out of guests, Billing</p>
				<p>&bull; Concierge</p>
			</div>
		</div>
	</div>
	<br>
	<div class="row text-center">
		<div class="col-md-2"></div>
		<div class="col-md-4">
			<div class="container-fluid">
				<h2 id="fnt2">DFOS</h2><h3 id="fnt">Design and Fitout System</h3>
				<p>&bull; Custom Design and Planning</p>
				<p>&bull; Procurement, Fabrication and Installation of design elements</p>
				<p>&bull; Curation of the unit for turnover</p>
				<p>&bull; Procurement of Add-Ons if required</p>
			</div>
		</div>
		<div class="col-md-4">
			<div class="container-fluid">
				<h2 id="fnt2">FSPMS</h2><h3 id="fnt">Full Service Property Management System</h3>
				<p>&bull; Basic Maintenance System</p>
				<p>&bull; Leasing and Marketing System</p>
				<p>&bull; Hotel Operation System</p>
				<p>&bull; Design and Fitout System</p>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>

	<br><br><br>
	<br><br><br>
	<hr id="membersprofile">
	<br><br>
	<h3 class="title usefont animated wow zoomIn" data-wow-delay=".5s">Officers' Profile</h3>
	<br><br>
	<div class="row container-fluid">
		<div class="col-md-2"></div>
	<div  class="col-md-4 mail-grid-right animated wow slideInRight" data-wow-delay=".5s" >
		<div class="mail-grid-right1" style="height:600px;">
			<img src="images/liz.png" alt=" " class="img-responsive" />
			<h4>Maria Liza V. Fresnido <span>Professional Real Estate Broker,
PRC Licence No.  001856405</span></h4>
			<p>
				&bull; President & CEO of Domus Fortem Corporation (Cebu-based) <br>
				&bull; President & CEO of Collyer Quay Corporation (Singapore-based)  <br>
				&bull; Treasurer of Buona Vista Prime Properties Corporation (Manila-based)  <br>
				&bull; Engaged in Real Estate Property Consulting, Property Management, Leasing Services and in Buying and Selling of Real Estate properties <br>
				&bull; Supports the group’s advocacy and active involvement in educating Filipinos in financial awareness and education through a campaign called, “Asensadong Pinoy”.  <br>
				&bull; Human Resource & Organization Development Consultant
			</p>
			<br>
			<ul class="phone-mail">
<!--				<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +1234 567 893</li>-->
				<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:lizfresnido@domusfortem.com">lizfresnido@domusfortem.com</a></li>
			</ul>
<!--			<ul class="social-nav model-8">-->
<!--				<li><a href="#" class="facebook"><i></i></a></li>-->
<!--				<li><a href="#" class="twitter"><i> </i></a></li>-->
<!--				<li><a href="#" class="g"><i></i></a></li>-->
<!--				<li><a href="#" class="p"><i></i></a></li>-->
<!--			</ul>-->
		</div>
	</div>
	<div class="col-md-4 mail-grid-right animated wow slideInRight" data-wow-delay=".5s">
		<div class="mail-grid-right1 " style="height:600px;">
			<img src="images/ian.png" alt=" " class="img-responsive" />
			<h4>IAN V. FRESNIDO <span>Vice President for Operations</span></h4>
			<p>
				&bull; Vice-President for Operations, Domus Fortem Corporation <br> <br>
				&bull; Interior Designer/Consultant – IVF Designs and Productions <br><br>
				&bull; Events Coordinator for Conventions/Exhibitions <br><br>
				&bull; Hotel Manager (2015) – Azia Suites and Residences <br><br>
				&bull; Tourism LGU Consultant <br><br>
				&bull; Advocate of “ASENSADONG PINOY”, a campaign on educating Filipinos in FINANCIAL AWARENESS AND LITERACY
			</p>
			<br>
			<ul class="phone-mail">
<!--				<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +1234 567 893</li>-->
				<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:ianfresnido@domusfortem.com">ianfresnido@domusfortem.com</a></li>
			</ul>
<!--			<ul class="social-nav model-8">-->
<!--				<li><a href="#" class="facebook"><i></i></a></li>-->
<!--				<li><a href="#" class="twitter"><i> </i></a></li>-->
<!--				<li><a href="#" class="g"><i></i></a></li>-->
<!--				<li><a href="#" class="p"><i></i></a></li>-->
<!--			</ul>-->
		</div>
	</div>
		<div class="col-md-2"></div>
	</div>

	<br><br><br>
	<br><br><br>
	<hr id="orgchart">
	<br><br>
	<h3 class="title usefont animated wow zoomIn" data-wow-delay=".5s">Our Team</h3>
	<br><br>

		<center>
			<img src="images/orgchart.png" alt=""  class="img-responsive text-center">
		</center>


	<?php include "footer.php";?>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 var defaults = {
		 containerID: 'toTop', // fading element id
		 containerHoverID: 'toTopHover', // fading element hover id
		 scrollSpeed: 1200,
		 easingType: 'linear'
		 };
		 */

		$().UItoTop({ easingType: 'easeOutQuart' });

	});
</script>
<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
	});
</script>
	<script type="text/javascript">
		$("#aboutus").addClass("menu__item--current");
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>