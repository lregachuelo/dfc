<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();
//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<?php include("head.php");?>
	<!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->
</head>

<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
<div class="banner1">
	<div class="container">
		<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Home</a> / <span>Leasing Services</span></h2>
	</div>
</div>
<!-- //banner -->
<!-- single -->
		<div class="">

			<div class="col-md-8">
				<div class="gallery ">
				<?php if(isset($_GET['viewleasing'])){
					$pid = $_GET['viewleasing'];

					?>
						<div class="gallery-grids">

							<?php
							$sql3 = "select * from leasing_images where img_pid='$pid' ORDER BY RAND() LIMIT 5";
							$res3 = $conn->query($sql3);
							while ($row2 = $res3->fetch_assoc()){
								?>
								<div class="gallery-grids1">
									<div class="gallery-grid-left animated wow slideInUp" data-wow-delay=".5s">
										<a href="leasing/<?php echo $row2['img_name'];?>" class="big">
											<img src="leasing/<?php echo $row2['img_name'];?>"  style="width: 640px;height: 190px;" alt=" " title="" class="img-responsive" />
										</a><br>
									</div>
								</div>

							<?php } ?>
						</div>

					<?php



					$sql = "SELECT * FROM leasing where prod_id='$pid'";
					$result = $conn->query($sql);
					while($row = $result->fetch_assoc()){
					?>
					<br>
					<br>
					<div class="flex-slider-single animated wow slideInLeft" data-wow-delay=".5s">
						<section class="slider">
							<div class="flexslider">
								<ul class="slides">
									<li>
										<div class="single-grid-left-grid">
											<img src="leasing/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="height: 500px" />


											<div class="single-grid-left-grid1">
												<div class="single-grid-left-grid1-left">
													<h3><i class="glyphicon glyphicon-star"></i> <span>PHP <?php echo $row['prod_price'];?></span></h3>
												</div>
												<div class="single-grid-left-grid1-right">
													<h4><?php echo $row['prod_name'];?></h4>
													<p> <?php echo $row['prod_address'];?></p>
												</div>
												<div class="clearfix"> </div>
												<p class="fugiat text-justify">

													<?php echo $row['prod_desc'];?>
												</p>



												PLACES NEARBY:
												<ul>
													<!--													<li>PLACES NEARBY</li>-->
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_school'];?>"
															data-html="true"><span class="glyphicon glyphicon-book" aria-hidden="true"></span>School</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_transpo'];?>"
															data-html="true"><span class="glyphicon glyphicon-transfer" aria-hidden="true"></span>Transpo</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_sm'];?>"
															data-html="true"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>Supermarket</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_mall'];?>"
															data-html="true"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span>Malls</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_resto'];?>"
															data-html="true"><span class="glyphicon glyphicon-cutlery" aria-hidden="true"></span>Resto</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_hospital'];?>"
															data-html="true"><span class="glyphicon glyphicon-bed" aria-hidden="true"></span>Hospital</a>
													</li>
													<li><a  tabindex="0" data-placement="bottom" role="button" data-toggle="popover" data-trigger="focus" title="What's Nearby"
															data-content="<?php echo $row['prod_church'];?>"
															data-html="true"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>Church</a>
													</li>

												</ul>
											</div>
										</div>
									</li>
							</div>
						</section>

					</div>

					<br><br>
					<?php  } ?>
				<?php  } ?>

				<?php if(isset($_GET['viewlist'])){
					$cid = $_GET['viewlist'];
					$sql1 = "SELECT catname FROM category ";
					$rs = $conn->query($sql1);
					?>
					<h3 class="title" id="fntAmatic">Leasing Services</h3>
				<div class="popular-posts" id="cbd">

					<?php
					$sql = "SELECT * FROM leasing";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
					// output data of each row
					while($row = $result->fetch_assoc()) {
					?>
					<div class="popular-posts-grids">


							<div class="col-md-4 popular-posts-grid animated wow slideInLeft" data-wow-delay=".5s">
								<div class="popular-posts-grid1">
									<a href="leasing.php?viewleasing=<?php echo $row['prod_id'];?>"><img src="leasing/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="width: 640px;height: 205px;"  /></a>
									<h4><a href="leasing.php?viewleasing=<?php echo $row['prod_id'];?>"><?php echo strtoupper($row['prod_name']) ;?></a></h4>
									<p class="text-center" style="height:75px;"><?php echo $row['prod_address'];?> <br><br></p>
								</div>
							</div>

					</div>
							<?php } ?>
						<?php }else{
							?>
							<p class="animated wow slideInLeft">
								No record found in this category.
							</p>
							<?php
								} ?>

				</div>
				<?php } ?>

			</div>
			</div>

			<div class="col-md-4 single-grid-right">

				<br><br>
				<div class=" mail-grid-right animated wow slideInRight container-fluid " style="background-color: #c1e2b3"data-wow-delay=".5s">
					<div class="mail-grid-right1"  style="background-color: #c1e2b3">
						<h3>QUICK NAVIGATION</h3>
						<hr>

							<h4 class="text-left"><a href="leasing.php?viewlist">LEASING SERVICES FOR</a></h4>
							<select name="" id="" onChange="document.location = this.value" value="GO" class="form-control">
								<?php
								$catid = $r['cat_id'];
								$sql = "SELECT * FROM leasing ";
								$result = $conn->query($sql);
								if ($result->num_rows > 0) {
									?>
									<option >--SELECT ITEM--</option>

									<?php
									while($row = $result->fetch_assoc()) {
										?>
										<option value="leasing.php?viewleasing=<?php echo $row['prod_id'];?>" <?php if($_GET['viewleasing'] == $row['prod_id']){ ?> selected <?php } ?>><?php echo $row['prod_name'];?></option>

										<?php
									}
								} else {
									?>
									<option value="" >Nothing to display</option>
									<?php
								} ?>
							</select>


					</div>
				</div>

				<br><br>

				<div class=" mail-grid-right animated wow slideInRight container-fluid " style="background-color: #444444" data-wow-delay=".5s">
					<div class="mail-grid-right1"  style="background-color: #444444">
						<img src="images/mandy.png" alt=" " class="img-responsive" />
						<h4 style="color: #00ffb9">Normandy Fresnido <span>Contact Person</span></h4>
						<br>
						<ul class="phone-mail">
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +63927-880-2393 | +63920-559-7833 </li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:mandyfresnido@domusfortem.com">mandyfresnido@domusfortem.com</a></li>
						</ul>
                                                <hr>

						<img src="images/ian.png" alt=" " class="img-responsive" />
						<h4 style="color: #00ffb9">Ian Fresnido <span>Contact Person</span></h4>
						<br>
						<ul class="phone-mail">
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +63927-482-3439 | +63923-104-3573 </li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:ianfresnido@domusfortem.com">ianfresnido@domusfortem.com</a></li>
						</ul>

					</div>
				</div>




			</div>
			<div class="clearfix"> </div>
		</div>
<!-- //single -->
<!-- footer -->
<?php include "footer.php";?>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript" src="js/simple-lightbox.min.js"></script>
<script>
	$(function(){
		var gallery = $('.gallery a').simpleLightbox({navText:		['&lsaquo;','&rsaquo;']});
	});
</script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
	$(window).load(function(){
		$('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
				$('body').removeClass('loading');
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		/*
		 var defaults = {
		 containerID: 'toTop', // fading element id
		 containerHoverID: 'toTopHover', // fading element hover id
		 scrollSpeed: 1200,
		 easingType: 'linear'
		 };
		 */

		$().UItoTop({ easingType: 'easeOutQuart' });

	});
</script>
<script type="text/javascript">
	$("#products").addClass("menu__item--current");
</script>

<!-- //here ends scrolling icon -->
<!-- //here ends scrolling icon -->
</body>
</html>