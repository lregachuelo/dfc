<style>
	.dropdown-submenu {
		position: relative;
	}

	.dropdown-submenu .dropdown-menu {
		top: 0;
		left: 100%;
		margin-top: -1px;
	}
</style>


<div class="header">
		<div class="container">
			<nav class="navbar navbar-default">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<div class="logo" style="padding-top: 12px">
							<a class="" href="index.php"><img src="images/brand2.jpg" alt="" class="img-responsive" style="max-width: 750px; max-height: 60px; display: block;width: auto;height: auto;"></a>
					</div>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<?php if ($_SESSION['logged']=="member") { ?>

				<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--francisco">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item" id="home"><a href="index.php" class="menu__link"><span class="menu__helper">Home</span></a></li>
							<li class="dropdown " style="">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp; Products <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="productsandservices.php"  data-target=".login" class="">FOR SELLING</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="leasing.php">FOR LEASING</a></li>
								</ul>
							</li>
							<li class="menu__item" id="products"><a href="fitoutservices.php" class="menu__link"><span class="menu__helper">&nbsp; &nbsp; Fitout Services</span></a></li>
							<li class="menu__item" id="products"><a href="events.php?eventlist" class="menu__link"><span class="menu__helper">EVENTS</span></a></li>
							<li class="dropdown " style="">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp;About Us <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="aboutus.php#whoweare">Company Profile</a></li>
									<li><a href="aboutus.php#orgchart">Company Organizational Chart</a></li>
									<li><a href="aboutus.php#membersprofile">Resumes of the Officers</a></li>
							</li>
							<li class="menu__item" id="about"><a href="db.php?logout" onclick="return confirm('Are you sure you want to log out of your account?');" class="menu__link"><span class="menu__helper"></span>Log out</a></li>
						</ul>
					</nav>
				</div>

				<?php }elseif ($_SESSION['logged']=="admin") { ?>

					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<nav class="menu menu--francisco">
							<ul class="nav navbar-nav menu__list">
								<li class="menu__item" id="home"><a href="index.php" class="menu__link"><span class="menu__helper">Home</span></a></li>
								<li class="dropdown " style="">
									<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp; Products <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="productsandservices.php"  data-target=".login" class="">FOR SELLING</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="leasing.php">FOR LEASING</a></li>
									</ul>
								</li>
								<li class="menu__item" id="products"><a href="fitoutservices.php" class="menu__link"><span class="menu__helper">&nbsp; &nbsp; Fitout Services</span></a></li>
								<li class="menu__item" id="products"><a href="events.php?eventlist" class="menu__link"><span class="menu__helper">EVENTS</span></a></li>
								<li class="dropdown " style="">
									<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp;About Us <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="aboutus.php#whoweare">Company Profile</a></li>
										<li><a href="aboutus.php#orgchart">Company Organizational Chart</a></li>
										<li><a href="aboutus.php#membersprofile">Resumes of the Officers</a></li>
									</ul>
								</li>
								<li class="menu__item" id="about"><a href="db.php?logout" onclick="return confirm('Are you sure you want to log out of your account?');" class="menu__link"><span class="menu__helper"></span>Log out</a></li>
							</ul>
						</nav>
					</div>
				<?php }else{ ?>
					<div class="collapse navbar-collapse nav-wil pull-right" id="bs-example-navbar-collapse-1">
					<nav class="menu menu--francisco">
						<ul class="nav navbar-nav menu__list">
							<li class="menu__item " id="home"><a href="index.php" class="menu__link"><span class="menu__helper">Home</span></a></li>
							<li class="dropdown" style="" id="products">
								<a href="#" class="dropdown-toggle  menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp; Products <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li class="dropdown-submenu">
										<a class="test" tabindex="-1" href="#">PROPERTIES <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
										<ul class="dropdown-menu">
											<li><a tabindex="-1" href="productsandservices.php">FOR SALE</a></li>
											<li><a tabindex="-1" href="leasing.php?viewlist">FOR LEASE</a></li>
										</ul>
									</li>
									<li><a href="services.php">SERVICES</a></li>
									<li><a href="fitoutservices.php">FITOUT SERVICES</a></li>
								</ul>
							</li>
<!--							<li class="menu__item" id="fitout"><a href="fitoutservices.php" class="menu__link"><span class="menu__helper">&nbsp; &nbsp; Fitout Services</span></a></li>-->
							<li class="menu__item" id="events"><a href="events.php?eventlist" class="menu__link"><span class="menu__helper">EVENTS</span></a></li>
							<li class="dropdown " style="" id="aboutus">
								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp;About Us <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="aboutus.php#whoweare">Company Profile</a></li>
									<li><a href="aboutus.php#orgchart">Company Organizational Chart</a></li>
									<li><a href="aboutus.php#membersprofile">Resumes of the Officers</a></li>
								</ul>
							</li>
							<li class="menu__item" id=""><a href="http://webmail.domusfortem.com/auth" target="_blank" class="menu__link"><span class="menu__helper">EMAIL LOGIN</span></a></li>

<!--														<li class="dropdown " style="">-->
<!--								<a href="#" class="dropdown-toggle menu__link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; Account <span class="caret"></span></a>-->
<!--								<ul class="dropdown-menu">-->
<!--									<li><a href="" data-toggle="modal" data-target=".login" class="">LOG IN</a></li>-->
<!--									<li role="separator" class="divider"></li>-->
<!--									<li><a href="signup.php">SIGN UP</a></li>-->
<!--								</ul>-->
<!--							</li>-->
						</ul>
					</nav>
					<!--					<div class="phone">-->
					<!--						<p><i class="glyphicon glyphicon-phone" aria-hidden="true"></i>Call- to -us <span>+0123 456 789</span></p>-->
					<!--					</div>-->
				</div>
				<?php } ?>


				<!-- /.navbar-collapse -->
			</nav>
		</div>
	</div>
<!-- //header -->

<div class="modal fade login" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">LOG IN</h4>
			</div>
			<form role="form" method="post" >
				<div class="modal-body">
					<div class="form-group"  >
						<input class="form-control" placeholder="Username" name="username" type="text" autofocus >
					</div>
					<div class="form-group">
						<input class="form-control" placeholder="Password" name="password" type="password" value="">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input class="btn btn-success" type="submit" value="Login" name="admin_login" >
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.dropdown a.test').on("click", function(e){
			$(this).next('ul').toggle();
			e.stopPropagation();
			e.preventDefault();
		});
	});
</script>
