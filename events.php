<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();

//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>

	<?php include("head.php");?>
	<style>
		.carousel-inner > .item > img,
		.carousel-inner > .item > a > img {
			width: 100%;
			margin: auto;
		}
	</style>
</head>
	
<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
	<div class="banner1">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
			<div class="col-md-4 bg-success">
				<ul class="nav nav-pills green">
					<li class="active"><a data-toggle="pill" href="#menu1">FOR SALE</a></li>
					<li><a data-toggle="pill" href="#menu2">FOR LEASE</a></li>
				</ul>
				<div class="tab-content">
					<div id="menu1" class="tab-pane fade in active">
						<br>
						<div class="row">
							<div class="col-md-9">
								<form action="" method="post">
								<div class="input-group">
									  <span class="input-group-btn">
										<button class="btn btn-default" type="submit" name="salesearch">Go!</button>
									  </span>
									<input type="text" name="sale" class="form-control" onkeyup="submit" placeholder="type your desired location...">
								</div><!-- /input-group -->
								</form>
							</div><!-- /.col-lg-6 -->
						</div><!-- /.row -->

					</div>
					<div id="menu2" class="tab-pane fade">
						<br>
						<div class="row">
							<div class="col-md-9">
								<form action="" method="post">
									<div class="input-group">
									  <span class="input-group-btn">
										<button class="btn btn-default" type="submit" name="leasesearch">Go!</button>
									  </span>
										<input type="text" name="sale" class="form-control" onkeyup="submit" placeholder="type your desired location...">
									</div><!-- /input-group -->
								</form>
							</div><!-- /.col-lg-6 -->
						</div><!-- /.row -->
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- single -->
	<div class="single">
		<div class="container">
			<div class="single-grids">
				<div class="col-md-8 single-grid-left">


					<?php
					if (isset($_POST['salesearch'])){
						$searchthis = $_POST['sale'];
						$sql = "SELECT * FROM products where prod_address LIKE '%$searchthis%' ORDER BY prod_name ASC";
						$res = $conn->query($sql);
						$count = $res->num_rows;
						if ($count == 0){
							echo "NO RESULT FOR THIS SEARCH...";
						}else{
							while ($row = $res->fetch_assoc()){
								?>

								<div class="col-md-4 popular-posts-grid animated wow slideInLeft " data-wow-delay=".5s">
									<div class="popular-posts-grid1">
										<a href="upload/<?php echo $row['prod_image'];?>"><img src="upload/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="width: 640px;height: 205px;"  /></a>
										<h4 style="height:70px;"><a href="products.php?viewprod=<?php echo $row['prod_id'];?>"><?php echo $row['prod_name'];?></a></h4>
										<p style="height:75px;"><?php echo $row['prod_address'];?></p>
											<hr>
									</div>
								</div>


								<?php
							}
						}
						?>

						<?php
					}elseif (isset($_POST['leasesearch'])) {
						$searchthis = $_POST['sale'];
						$sql = "SELECT * FROM leasing where prod_address LIKE '%$searchthis%' ORDER BY prod_name ASC";
						$res = $conn->query($sql);
						$count = $res->num_rows;
						if ($count == 0) {
							echo "NO RESULT FOR THIS SEARCH...";
						} else {
							while ($row = $res->fetch_assoc()) {
								?>

								<div class="col-md-4 popular-posts-grid animated wow slideInLeft " data-wow-delay=".5s">
									<div class="popular-posts-grid1">
										<div class="container-fluid">
											<a href="upload/<?php echo $row['prod_image']; ?>"><img
													src="upload/<?php echo $row['prod_image']; ?>" alt=" "
													class="img-responsive" style="width: 640px;height: 205px;"/></a>
											<h4 style="height:70px;"><a
													href="leasing.php?viewleasing=<?php echo $row['prod_id']; ?>"><?php echo $row['prod_name']; ?></a>
											</h4>
											<p style="height:75px;"><?php echo $row['prod_address']; ?></p>
											<hr>
										</div>
									</div>
								</div>


								<?php
							}
						}
					}else{
						?>

					<?php if (isset($_GET['eventlist'])){ ?>
					<h3 class="title animated wow zoomIn" id="fntAmatic" data-wow-delay=".5s">Events</h3>
					<br>




						<div class="related-posts animated wow slideInUp">
							<?php
							$sql = "SELECT * FROM events order by `date` DESC ";
							$rs = $conn->query($sql);
							$count = $rs->num_rows;
							if ($count > 0){
								while ($row = $rs->fetch_assoc()){

									?>
									<div class="related-post animated wow slideInUp" data-wow-delay=".5s">
										<div class="related-post-left">
											<a href="events.php?viewevent=<?=$row['id']?>"><img src="events/<?php echo $row['image'];?>" alt=" " class="img-responsive" /></a>
										</div>
										<div class="related-post-right">
											<h4><a href="#"><?php echo $row['title'];?></a></h4>
											<p>
												<span><?=$row['details']?></span>
											</p>
											<ul>
												<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?=$row['timefrom']." - ".$row['timeto']?></li>
												<br>
												<li><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?=$row['date']?></li>
												<br>
												<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?=$row['venue']?></li>
											</ul>
										</div>
										<div class="clearfix"> </div>
									</div>
									<?php
								}
							}else{
								echo "NO EVENTS CREATED.";
							}
							?>
						</div>

					<?php } ?>


<!--					<div class="popular-posts">-->
<!--						<h3 class="animated wow zoomIn" id="fntAmatic32" data-wow-delay=".5s">More Videos</h3>-->
<!--						<div class="popular-posts-grids">-->
<!--							<div class="popular-posts-grid animated wow slideInLeft" data-wow-delay=".5s">-->
<!--								--><?php
//								$sql = "SELECT * FROM videos LIMIT 3";
//								$r = $conn->query($sql);
//								while($row = $r->fetch_assoc()){
//								?>
<!--								<div class="popular-posts-grid1 col-md-4 ">-->
<!--									<a href=""><img src="videos/--><?php //echo $row['vid_thumbpic'];?><!--" alt=" " class="img-responsive" /></a>-->
<!--									<h4><a href="">--><?//=$row['vid_title']?><!--</a></h4>-->
<!--									<p>--><?//=$row['vid_detail']?><!--</p>-->
<!--								</div>-->
<!--								--><?php //} ?>
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
					<?php } ?>
				</div>


				<div class="col-md-4 single-grid-right">
					<div class="blog-right1">
						<div class="categories animated wow slideInUp" data-wow-delay=".5s">
							<h3 id="fntAmatic32">domus fortem corporation</h3>
							<div class="container-fluid">
								<blockquote>"We promise to keep building and managing
									preferred condominiums in the most
									desired locations in the country."</blockquote>
							</div>
							<ul>
								<li><a href="#"> Carries projects of Vista land properties under Vista Residences</a></li>
								<li><a href="#"> Currently servicing in Manila, Cebu and Mindanao</a></li>
							</ul>
						</div>

						<div class="related-posts animated wow slideInUp">
							<h3 id="fntAmatic32" class="animated wow slideInUp">Upcoming Events <a href="events.php?eventlist" class="text-success">&nbsp;&nbsp;[ see all ]</a></h3>
							<?php
								$sql = "SELECT * FROM events LIMIT 4";
								$rs = $conn->query($sql);
								$count = $rs->num_rows;
								if ($count > 0){
									while ($row = $rs->fetch_assoc()){

							?>
							<div class="related-post animated wow slideInUp" data-wow-delay=".5s">
								<div class="related-post-left">
									<a href="events.php?viewevent=<?=$row['id']?>"><img src="events/<?php echo $row['image'];?>" alt=" " class="img-responsive" /></a>
								</div>
								<div class="related-post-right">
									<h4><a href="events.php?viewevent=<?=$row['id']?>"><?php echo $row['title'];?></a></h4>
									<p>
										<span><?=$row['details']?></span>
									</p>
									<ul>
										<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?=$row['timefrom']." - ".$row['timeto']?></li>
										<br>
										<li><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?=$row['date']?></li>
										<br>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?=$row['venue']?></li>
									</ul>
								</div>
								<div class="clearfix"> </div>
							</div>
							<?php
								}
							}else{
									echo "NO EVENTS CREATED.";
								}
							?>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //single -->
<!-- footer -->
	<div class="footer">	
		<div class="container">

			<div class="footer-grids1">
				<div class="footer-grids1-left animated wow slideInLeft" data-wow-delay=".5s">
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About Us</a></li>
						<li><a href="short-codes.php">Short Codes</a></li>
						<li><a href="gallery.php">Gallery</a></li>
						<li><a href="mail.php">Mail Us</a></li>
					</ul>
				</div>
				<div class="footer-grids1-right">
					<p class="animated wow slideInRight" data-wow-delay=".5s">&copy 2016 Acreage. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<script type="text/javascript">
		$("#events").addClass("menu__item--current");
	</script>


<!-- //here ends scrolling icon -->
</body>
</html>