<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();

//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<?php include("head.php");?>

</head>
	
<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
	<div class="banner1">
		<div class="container">
			<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Home</a> / <span>Sign Up</span></h2>
		</div>
	</div>
<!-- //banner -->
<!-- mail -->
	<div class="mail">
		<div class="container">
			<h3 class="animated wow zoomIn" data-wow-delay=".5s">Sign Up</h3>
			<p class="qui animated wow zoomIn" data-wow-delay=".5s">Create an account with us to enjoy the full features we offer.</p>
			<div class="mail-grids">
				<div class="col-md-8 mail-grid-left animated wow slideInLeft" data-wow-delay=".5s">
					<form method="post" class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

						<div class="form-group">
							<label for="">First Name:</label>
							<input type="text" class="form-control" name="firstname" value="<?php echo $firstname;?>">
						</div>

						<div class="form-group">
							<label for="">Middle Name:</label>
							<input type="text" class="form-control" name="middlename">
						</div>

						<div class="form-group">
							<label for="">Last Name:</label>
							<input type="text" class="form-control" name="lastname">
						</div>

						<div class="form-group">
							<label for="">Gender:</label><br>
							<input type="radio" name="gender" value="male" > Male &nbsp;
							<input type="radio" name="gender" value="female"> Female<br>
						</div>

						<div class="form-group">
							<label for="">Address:</label>
							<input type="text" class="form-control" name="address">
						</div>

						<div class="form-group">
							<label for="">Contact Number</label>
							<input type="number" class="form-control" name="contactno">
						</div>

						<div class="form-group">
							<label for="">Email:</label>
							<input type="email" class="form-control" name="email">
						</div>

						<div class="form-group">
							<label for="">Username:</label><span class="text-danger">* <?php echo $unameErr;?></span>
							<input type="text" class="form-control" name="username">
						</div>

						<div class="form-group">
							<label for="">Password:</label>
							<input type="password" class="form-control" name="password">
						</div>

						<button type="submit" name="signup">SIGNUP</button>
 					</form>
				</div>
				<div class="col-md-4 mail-grid-right animated wow slideInRight" data-wow-delay=".5s">
					<div class="mail-grid-right1">
						xcvbnm,.
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>

		</div>
	</div>
<!-- //mail -->
<!-- footer -->
	<div class="footer">	
		<div class="container">
			<div class="footer-grids">
				<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".5s">
					<h3>About Us</h3>
					<p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse 
						quam nihil molestiae consequatur.</p>
					<div class="social">
						<ul class="social-nav model-8">
							<li><a href="#" class="facebook"><i></i></a></li>
							<li><a href="#" class="twitter"><i> </i></a></li>
							<li><a href="#" class="g"><i></i></a></li>
							<li><a href="#" class="p"><i></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".6s">
					<h3>Subscribe</h3>
					<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis.</p>
					<form action="#" method="post">
						<input type="email" name="Email" value="Enter Your Email..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email...';}" required="">
						<input type="submit" value="Send">
					</form>
				</div>
				<div class="col-md-4 footer-grid animated wow slideInLeft" data-wow-delay=".7s">
					<h3>Latest Tweets</h3>
					<ul class="footer-grid-list">
						<li>Nam libero tempore, cum soluta nobis est eligendi optio 
							cumque nihil impedit. <span>1 day ago</span></li>
						<li>Itaque earum rerum hic tenetur a sapiente delectus <a href="mailto:info@mail.com">KWEsa@mail.com</a>
							cumque nihil impedit. <span>1 day ago</span></li>
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="footer-grids1">
				<div class="footer-grids1-left animated wow slideInLeft" data-wow-delay=".5s">
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="about.php">About Us</a></li>
						<li><a href="short-codes.php">Short Codes</a></li>
						<li><a href="gallery.php">Gallery</a></li>
						<li class="active"><a href="mail.php">Mail Us</a></li>
					</ul>
				</div>
				<div class="footer-grids1-right">
					<p class="animated wow slideInRight" data-wow-delay=".5s">&copy 2016 Acreage. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>