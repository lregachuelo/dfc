<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();
//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>
	<?php include("head.php");?>

</head>
	
<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
	<div class="banner1">
		<div class="container">
			<h2 class="animated wow slideInLeft" data-wow-delay=".5s"><a href="index.php">Home</a> / <span>Fitout Services</span></h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-8">
			<div class="gallery ">
			<div class="single-grid-left-grid container-fluid">
				<?php
				$query = "SELECT * FROM fitout";
				$result = $conn->query($query);
				while($rf = $result->fetch_assoc()){
					?>
					<h3 class="animated wow zoomIn" id="fntAmatic"  data-wow-delay=".5s"><?php echo $rf['name'];?></h3>
					<br><br>
					<?php
				}
				?>

				<h4 id="fnt32" class="text-left animated wow zoomIn">Katharos Series</h4>
				<br>
				<div class="flex-slider-single animated wow slideInLeft" data-wow-delay=".5s">
					<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<?php
								$sql = "select * from fitout_images where fitout_id=1 and fitcat=1 ORDER BY RAND() LIMIT 5";
								$res = $conn->query($sql);
								while ($row = $res->fetch_assoc()){
								?>
								<li>
										<img src="fitout/<?php echo $row['name'];?>" alt=" " style="max-height: 500px;" class="img-responsive" />
								</li>
								<?php } ?>
							</ul>
						</div>
					</section>
					<!--FlexSlider-->
					<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
					<script defer src="js/jquery.flexslider.js"></script>
					<script type="text/javascript">
						$(window).load(function(){
							$('.flexslider').flexslider({
								animation: "slide",
								start: function(slider){
									$('body').removeClass('loading');
								}
							});
						});
					</script>
					<!--End-slider-script-->
				</div>


<!--					<p class="qui animated wow zoomIn" data-wow-delay=".5s">106 L.P. Leviste Street, Salcedo Village, Makati City, 1227</p>-->
					<div class="gallery-grids">

						<?php
							$sql = "select * from fitout_images where fitout_id=1 and fitcat=1 ORDER BY RAND() LIMIT 5";
							$res = $conn->query($sql);
							while ($row = $res->fetch_assoc()){
						?>
						<div class="gallery-grids1">
							<div class="gallery-grid-left animated wow slideInUp" data-wow-delay=".5s">
								<a href="fitout/<?php echo $row['name'];?>" class="big">
									<img src="fitout/<?php echo $row['name'];?>"  style="width: 640px;height: 190px;" alt=" " title="<?php echo $row['caption'];?>" class="img-responsive" />
								</a><br>
							</div>
						</div>

							<?php } ?>
					</div>
				</div>
				<br>
				<hr>
				<br>
				<h4 id="fnt32" class="text-left animated wow zoomIn container-fluid">Omorphos Series</h4>
				<br>
				<div class="flex-slider-single animated wow slideInLeft container-fluid" data-wow-delay=".5s">
					<section class="slider">
						<div class="flexslider">
							<ul class="slides">
								<?php
								$sql = "select * from fitout_images where fitout_id=2 and fitcat=2 ORDER BY RAND() LIMIT 5";
								$res = $conn->query($sql);
								while ($row = $res->fetch_assoc()){
								?>
								<li>
										<img src="fitout/<?php echo $row['name'];?>" alt=" " style="max-height: 500px;" class="img-responsive" />
								</li>
								<?php } ?>
							</ul>
						</div>
					</section>
					<!--FlexSlider-->
					<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
					<script defer src="js/jquery.flexslider.js"></script>
					<script type="text/javascript">
						$(window).load(function(){
							$('.flexslider').flexslider({
								animation: "slide",
								start: function(slider){
									$('body').removeClass('loading');
								}
							});
						});
					</script>
					<!--End-slider-script-->
				</div>


<!--					<p class="qui animated wow zoomIn" data-wow-delay=".5s">106 L.P. Leviste Street, Salcedo Village, Makati City, 1227</p>-->
					<div class="gallery-grids">

						<?php
							$sql = "select * from fitout_images where fitout_id=2 and fitcat=2 ORDER BY RAND() LIMIT 5";
							$res = $conn->query($sql);
							while ($row = $res->fetch_assoc()){
						?>
						<div class="gallery-grids1">
							<div class="gallery-grid-left animated wow slideInUp" data-wow-delay=".5s">
								<a href="fitout/<?php echo $row['name'];?>" class="big">
									<img src="fitout/<?php echo $row['name'];?>"  style="width: 640px;height: 190px;" alt=" " title="<?php echo $row['caption'];?>" class="img-responsive" />
								</a><br>
							</div>
						</div>

							<?php } ?>
					</div>
				</div>
				<br><br><br>
				<br><br><br>

			</div>

		<div class="col-md-4">
			<br><br><br><br>
			<h3 id="fntAmatic" class="text-center">Fitout Services</h3><br>
			<div class="container-fluid">
				<h3 id="fnt32">Design and Fitout System</h3>
				<p>&bull; Custom Design and Planning</p>
				<p>&bull; Procurement, Fabrication and Installation of design elements</p>
				<p>&bull; Curation of the unit for turnover</p>
				<p>&bull; Procurement of Add-Ons if required</p>
			</div>

			<hr>

			<div class=" mail-grid-right animated wow slideInRight container-fluid " style="background-color: #444444" data-wow-delay=".5s">
				<div class="mail-grid-right1"  style="background-color: #444444">
					<img src="images/mandy.png" alt=" " class="img-responsive" />
					<h4 style="color: #00ffb9">Normandy Fresnido <span>Contact Person</span></h4>
					<br>
					<ul class="phone-mail">
						<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +63927-880-2393 | +63920-559-7833 </li>
						<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:mandyfresnido@domusfortem.com">mandyfresnido@domusfortem.com</a></li>
					</ul>

                                                <hr>

						<img src="images/ian.png" alt=" " class="img-responsive" />
						<h4 style="color: #00ffb9">Ian Fresnido <span>Contact Person</span></h4>
						<br>
						<ul class="phone-mail">
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone: +63927-482-3439 | +63923-104-3573 </li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email: <a href="mailto:ianfresnido@domusfortem.com">ianfresnido@domusfortem.com</a></li>
						</ul>

				</div>
			</div>

		</div>
	</div>

<?php include "footer.php";?>
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript" src="js/simple-lightbox.min.js"></script>
	<script>
		$(function(){
			var gallery = $('.gallery a').simpleLightbox({navText:		['&lsaquo;','&rsaquo;']});
		});
	</script>
	<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
	<script defer src="js/jquery.flexslider.js"></script>
	<script type="text/javascript">
		$(window).load(function(){
			$('.flexslider').flexslider({
				animation: "slide",
				start: function(slider){
					$('body').removeClass('loading');
				}
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<script type="text/javascript">
	$("#fitout").addClass("menu__item--current");
</script>

<!-- //here ends scrolling icon -->
</body>
</html>		