<?php
include "db.php";
$conn = new mysqli($servername, $username, $password, $dbname);
session_start();

//if ($_SESSION['logged']!="admin") {
//	header('Location: index.php');
//}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Domus Fortem Corporation</title>

	<?php include("head.php");?>
	<style>
		.carousel-inner > .item > img,
		.carousel-inner > .item > a > img {
			width: 100%;
			margin: auto;
		}
	</style>

	<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<script type="text/javascript" src="engine1/jquery.js"></script>
	<!-- End WOWSlider.com HEAD section -->

</head>

<body>
<!-- header -->
<?php include("header.php");?>
<!-- //header -->
<!-- banner -->
	<div class="banner1">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4"></div>
			<div class="col-md-4 bg-success">
				<ul class="nav nav-pills green">
					<li class="active"><a data-toggle="pill" href="#menu1">FOR SALE</a></li>
					<li><a data-toggle="pill" href="#menu2">FOR LEASE</a></li>
				</ul>
				<div class="tab-content">
					<div id="menu1" class="tab-pane fade in active">
						<br>
						<div class="row">
							<div class="col-md-9">
								<form action="" method="post">
								<div class="input-group">
									  <span class="input-group-btn">
										<button class="btn btn-default" type="submit" name="salesearch">Go!</button>
									  </span>
									<input type="text" name="sale" class="form-control" onkeyup="submit" placeholder="type your desired location...">
								</div><!-- /input-group -->
								</form>
							</div><!-- /.col-lg-6 -->
						</div><!-- /.row -->

					</div>
					<div id="menu2" class="tab-pane fade">
						<br>
						<div class="row">
							<div class="col-md-9">
								<form action="" method="post">
									<div class="input-group">
									  <span class="input-group-btn">
										<button class="btn btn-default" type="submit" name="leasesearch">Go!</button>
									  </span>
										<input type="text" name="sale" class="form-control" onkeyup="submit" placeholder="type your desired location...">
									</div><!-- /input-group -->
								</form>
							</div><!-- /.col-lg-6 -->
						</div><!-- /.row -->
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- single -->
	<div class="single">
		<div class="container">
			<div class="row">
				<div class="col-md-8 single-grid-left">


					<?php
					if (isset($_POST['salesearch'])){
						$searchthis = $_POST['sale'];
						$sql = "SELECT * FROM products where prod_address LIKE '%$searchthis%' ORDER BY prod_name ASC";
						$res = $conn->query($sql);
						$count = $res->num_rows;
						if ($count == 0){
							echo "NO RESULT FOR THIS SEARCH...";
						}else{
							while ($row = $res->fetch_assoc()){
								?>

								<div class="col-md-4 popular-posts-grid animated wow slideInLeft " data-wow-delay=".5s">
									<div class="popular-posts-grid1">
										<a href="upload/<?php echo $row['prod_image'];?>"><img src="upload/<?php echo $row['prod_image'];?>" alt=" " class="img-responsive" style="width: 640px;height: 205px;"  /></a>
										<h4 style="height:70px;"><a href="products.php?viewprod=<?php echo $row['prod_id'];?>"><?php echo $row['prod_name'];?></a></h4>
										<p style="height:75px;"><?php echo $row['prod_address'];?></p>
											<hr>
									</div>
								</div>


								<?php
							}
						}
						?>

						<?php
					}elseif (isset($_POST['leasesearch'])) {
						$searchthis = $_POST['sale'];
						$sql = "SELECT * FROM leasing where prod_address LIKE '%$searchthis%' ORDER BY prod_name ASC";
						$res = $conn->query($sql);
						$count = $res->num_rows;
						if ($count == 0) {
							echo "NO RESULT FOR THIS SEARCH...";
						} else {
							while ($row = $res->fetch_assoc()) {
								?>

								<div class="col-md-4 popular-posts-grid animated wow slideInLeft " data-wow-delay=".5s">
									<div class="popular-posts-grid1">
										<div class="container-fluid">
											<a href="leasing/<?php echo $row['prod_image']; ?>"><img
													src="leasing/<?php echo $row['prod_image']; ?>" alt=" "
													class="img-responsive" style="width: 640px;height: 205px;"/></a>
											<h4 style="height:70px;"><a
													href="leasing.php?viewleasing=<?php echo $row['prod_id']; ?>"><?php echo $row['prod_name']; ?></a>
											</h4>
											<p style="height:75px;"><?php echo $row['prod_address']; ?></p>
											<hr>
										</div>
									</div>
								</div>


								<?php
							}
						}
					}else{
						?>

					<h3 class="title animated wow zoomIn"id="fntAmatic"  data-wow-delay=".5s">Featured Video</h3>
					<br>



					<?php
					$sql = "SELECT * FROM videos where vid_isfeat = 1";
					$r = $conn->query($sql);
					while($row = $r->fetch_assoc()){
					?>
					<div class="flex-slider-single animated wow slideInLeft" data-wow-delay=".5s">
						<section class="slider">
							<div class="flexslider">
										<div class="single-grid-left-grid">
											<div class="embed-responsive embed-responsive-16by9"  >
												<iframe id="myVideo" class="embed-responsive-item" width="854" height="480" src="<?php echo $row['vid_link'];?>" frameborder="0" allowfullscreen></iframe>
											</div>
											<div class="single-grid-left-grid1">
												<div class="single-grid-left-grid1-left">
												</div>
												<div class="single-grid-left-grid1-right">
													<h4 id="fntAmatic"><?php echo $row['vid_title'];?></h4>
												</div>
												<div class="clearfix"> </div>

											</div>
										</div>
							</div>
						</section>
					</div>
					<?php } ?>
						<br><br>
						<hr>
						<br>
						<h3 id="fntAmatic" class="title">GALLERY</h3>
						<br><br>

						<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
						<div id="wowslider-container1">
							<div class="ws_images"><ul>
									<li><img src="data1/images/1.jpg" alt="1" title="1" id="wows1_0"/></li>
									<li><img src="data1/images/costa.jpg" alt="costa" title="costa" id="wows1_1"/></li>
									<li><img src="data1/images/image002010650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005v.jpg" alt="image-0-02-01-0650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005-V" title="image-0-02-01-0650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005-V" id="wows1_2"/></li>
									<li><img src="data1/images/image00201bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86v.jpg" alt="image-0-02-01-bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86-V" title="image-0-02-01-bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86-V" id="wows1_3"/></li>
									<li><img src="data1/images/image00201c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14v.jpg" alt="image-0-02-01-c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14-V" title="image-0-02-01-c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14-V" id="wows1_4"/></li>
									<li><img src="data1/images/image00201fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72v.jpg" alt="image-0-02-01-fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72-V" title="image-0-02-01-fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72-V" id="wows1_5"/></li>
									<li><img src="data1/images/katipunan.jpg" alt="katipunan" title="katipunan" id="wows1_6"/></li>
									<li><img src="data1/images/pine.png" alt="pine" title="pine" id="wows1_7"/></li>
									<li><img src="data1/images/recto.jpg" alt="recto" title="recto" id="wows1_8"/></li>
									<li><img src="data1/images/ssq.jpg" alt="ssq" title="ssq" id="wows1_9"/></li>
									<li><img src="data1/images/suarez.jpg" alt="suarez" title="suarez" id="wows1_10"/></li>
									<li><a href="http://wowslider.com"><img src="data1/images/taft.jpg" alt="jquery slider" title="taft" id="wows1_11"/></a></li>
									<li><img src="data1/images/trevi.jpg" alt="trevi" title="trevi" id="wows1_12"/></li>
								</ul></div>
							<div class="ws_bullets"><div>
									<a href="#" title="1"><span><img src="data1/tooltips/1.jpg" alt="1"/>1</span></a>
									<a href="#" title="costa"><span><img src="data1/tooltips/costa.jpg" alt="costa"/>2</span></a>
									<a href="#" title="image-0-02-01-0650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005-V"><span><img src="data1/tooltips/image002010650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005v.jpg" alt="image-0-02-01-0650e995fe3252e903c02260ac0219dc96e92f7b7fa8a19e77db72ea1ce16005-V"/>3</span></a>
									<a href="#" title="image-0-02-01-bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86-V"><span><img src="data1/tooltips/image00201bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86v.jpg" alt="image-0-02-01-bf4b71d119aa1a73934f7735443ec82eb5275f0ee7d49262bc0e3a3016d20e86-V"/>4</span></a>
									<a href="#" title="image-0-02-01-c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14-V"><span><img src="data1/tooltips/image00201c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14v.jpg" alt="image-0-02-01-c0d1fc5a8b93b3c529c504230c629beeab2388535af5a78f92589ed07eff2b14-V"/>5</span></a>
									<a href="#" title="image-0-02-01-fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72-V"><span><img src="data1/tooltips/image00201fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72v.jpg" alt="image-0-02-01-fc63db147bc56918187dc71feab261cfa65d4f840785a7059e31aa163dcccc72-V"/>6</span></a>
									<a href="#" title="katipunan"><span><img src="data1/tooltips/katipunan.jpg" alt="katipunan"/>7</span></a>
									<a href="#" title="pine"><span><img src="data1/tooltips/pine.png" alt="pine"/>8</span></a>
									<a href="#" title="recto"><span><img src="data1/tooltips/recto.jpg" alt="recto"/>9</span></a>
									<a href="#" title="ssq"><span><img src="data1/tooltips/ssq.jpg" alt="ssq"/>10</span></a>
									<a href="#" title="suarez"><span><img src="data1/tooltips/suarez.jpg" alt="suarez"/>11</span></a>
									<a href="#" title="taft"><span><img src="data1/tooltips/taft.jpg" alt="taft"/>12</span></a>
									<a href="#" title="trevi"><span><img src="data1/tooltips/trevi.jpg" alt="trevi"/>13</span></a>
								</div></div><div class="ws_script" style="position:absolute;left:-99%"><a href="http://wowslider.com">wowslider</a> by WOWSlider.com v8.7</div>
							<div class="ws_shadow"></div>
						</div>


<!--					<div class="popular-posts">-->
<!--						<h3 class="animated wow zoomIn" id="fntAmatic32" data-wow-delay=".5s">More Videos</h3>-->
<!--						<div class="popular-posts-grids">-->
<!--							<div class="popular-posts-grid animated wow slideInLeft" data-wow-delay=".5s">-->
<!--								--><?php
//								$sql = "SELECT * FROM videos LIMIT 3";
//								$r = $conn->query($sql);
//								while($row = $r->fetch_assoc()){
//								?>
<!--								<div class="popular-posts-grid1 col-md-4 ">-->
<!--									<a href=""><img src="videos/--><?php //echo $row['vid_thumbpic'];?><!--" alt=" " class="img-responsive" /></a>-->
<!--									<h4><a href="">--><?//=$row['vid_title']?><!--</a></h4>-->
<!--									<p>--><?//=$row['vid_detail']?><!--</p>-->
<!--								</div>-->
<!--								--><?php //} ?>
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
					<?php } ?>
				</div>

				<div class="col-md-4 single-grid-right">
					<div class="blog-right1">
						<div class="categories animated wow slideInUp" data-wow-delay=".5s">
							<a href="http://webmail.domusfortem.com/auth" target="_blank"><h3 id="fntAmatic32">domus fortem corporation</h3></a>
							<div class="container-fluid">
								<blockquote>"We promise to keep improving and managing
									preferred condominiums in the most
									desired locations in the country."</blockquote>
							</div>
							<ul>
								<li><a href="#"> Carries projects of Vista land properties under Vista Residences</a></li>
								<li><a href="#"> Currently servicing in Manila, Cebu and Mindanao</a></li>
							</ul>
						</div>

						<div class="related-posts animated wow slideInUp">
							<h3 id="fntAmatic32" class="animated wow slideInUp">Upcoming Events <a href="events.php?eventlist" class="text-success">&nbsp;&nbsp;[ see all ]</a></h3>
							<?php
								$sql = "SELECT * FROM events LIMIT 4";
								$rs = $conn->query($sql);
								$count = $rs->num_rows;
								if ($count > 0){
									while ($row = $rs->fetch_assoc()){

							?>
							<div class="related-post animated wow slideInUp" data-wow-delay=".5s">
								<div class="related-post-left">
									<a href="events.php?viewevent=<?=$row['id']?>"><img src="events/<?php echo $row['image'];?>" alt=" " class="img-responsive" /></a>
								</div>
								<div class="related-post-right">
									<h4><a href="events.php?viewevent=<?=$row['id']?>"><?php echo $row['title'];?></a></h4>
									<p>
										<span><?=$row['details']?></span>
									</p>
									<ul>
										<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span> <?=$row['timefrom']." - ".$row['timeto']?></li>
										<br>
										<li><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <?=$row['date']?></li>
										<br>
										<li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?=$row['venue']?></li>
									</ul>
								</div>
								<div class="clearfix"> </div>
							</div>
							<?php
								}
							}else{
									echo "NO EVENTS CREATED.";
								}
							?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

<!-- //single -->
<!-- footer -->
	<?php include "footer.php";?>
<!-- //footer -->
<!-- for bootstrap working -->
<script src="js/bootstrap.js"></script>
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
	<script type="text/javascript">
		$("#home").addClass("menu__item--current");
	</script>


<!-- //here ends scrolling icon -->
</body>
</html>